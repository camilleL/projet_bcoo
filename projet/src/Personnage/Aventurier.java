package Personnage;
/**
 * 
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Aventurier
 * Hérite de Personnage
 * Représente un aventurier
 */
public class Aventurier extends Personnage{
	
		//ATTRIBUT
	
	/** Tag, donne le type du personnage*/
	public final static String TAG = "PJ";
	/** boolean indiquant si le personnage a l'amulette */
	private boolean amulette = false;
	
	
		//CONSTRUCTEUR
	
	/**
	 * Constructeur à 1 paramètre
	 * @param nom	nom de l'aventurier
	 */
	public Aventurier(String nom){
		super(nom);
	}
	
	
	/**
	 * Constructeur à 1 paramètre
	 * @param nom	nom de l'aventurier
	 * @param vie 	vie de l'aventurier
	 */
	public Aventurier(String nom,int vie){
		super(nom,vie);
	}
	
	
		//METHODES
	
	/**
	 * Getter du nom du personnage
	 * @return
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Setter du nom du personnage
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Getter du Tag du personnage
	 * @return
	 */
	public String getTag() {
		return TAG;
	}
	
	/**
	 * Getter de amulette
	 * @return boolean indiquant si le joueur a l'amulette ou non
	 */
	public boolean getAmulette()
	{
		return this.amulette;
	}
	
	/**
	 * Setter de amulette
	 * @param boolean amulette
	 */
	public void setAmulette(boolean a)
	{
		this.amulette = a;
	}
}