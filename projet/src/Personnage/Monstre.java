package Personnage;

/**
 * Classe représentant un monstre
 * Hérite de Personnage
 */
public class Monstre extends Personnage
{
	//ATTRIBUT
	
		/** Tag, donne le type du personnage*/
		public final static String TAG = "M";

		
			//CONSTRUCTEUR
		
		/**
		 * Constructeur à 1 paramètre
		 * @param nom	nom de l'aventurier
		 */
		public Monstre(String nom){
			super(nom);
		}
		
		
			//METHODES
		
		/**
		 * Getter du nom du personnage
		 * @return
		 */
		public String getNom() {
			return nom;
		}

		/**
		 * Setter du nom du personnage
		 * @param nom
		 */
		public void setNom(String nom) {
			this.nom = nom;
		}

		/**
		 * Getter du Tag du personnage
		 * @return
		 */
		public String getTag() {
			return TAG;
		}
	}

