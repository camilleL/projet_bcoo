package Personnage;

/**
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe représentant la vie
 *
 */
public class Vie 
{
	/** Attribut représentant la valeur de la vie */
	private int valeur;
	
	
		//CONSTRUCTEURS
	
	/**
	 * Constructeur vide
	 * Met la vie par défaut à 5
	 */
	public Vie(){
		this.valeur = 5;
	}
	
	/**
	 * Constructeur vie
	 * @param int valeur de la vie
	 */
	public Vie(int v){
		if(v <= 0)
			v = 5;
		this.valeur = v;
	}
	
	/**
	 * Méthode permettant d'incrémenter la vie
	 */
	public void incrementer()
	{
		this.valeur += 1 ;
	}
	
	/** 
	 * Méthode permettant de decrementer la vie
	 */
	public void decrementer()
	{
		this.valeur -= 1;
		if(this.valeur <= 0)
			this.valeur = 0;
	}
	
	/**
	 * getter de la valeur de vie
	 * @return 
	 * @return vie
	 */
	public int getValeur()
	{
		return this.valeur;
	}
}