package Personnage;

import Case.*;
/**
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Personnage
 * Représente un personnage du jeu
 */
public class Personnage {

		//ATTRIBUTS
	
	/** Tableau des directions possibles*/
	private int[] direction;
	/** Case du labyrinthe où se trouve le personnage*/
	private Case espace;
	/** Retourne vrai seulement si on a pu bouger*/
	private boolean moved;
	/** Nom du personnage*/
	String nom;
	/** Tag, donne le type du personnage*/
	public final static String TAG = "P";
	/** Objet vie représentant la vie */
	private Vie v;
	/** boolean indiquant si le personnage est vivant ou pas **/
	private boolean vivant = true;
	/** String donnant la dernier direction connue **/
	private String derniereDirection;
	
	
		//CONSTRUCTEURS
	
	public Personnage(){
		this.nom = "Inconnu";
		
		//initialisation de la direction a rester sur place
		this.direction=new int[2];
		this.direction[0]=0;
		this.direction[1]=0;
		this.setDerniereDirection("S");
		this.moved = false; 
		this.v = new Vie(5); 
	}
	
	public Personnage(String name){
		if(name == null)
			name = "Inconnu";
		this.nom = name;
		
		//initialisation de la direction a rester sur place
		this.direction=new int[2];
		this.direction[0]=0;
		this.direction[1]=0;
		this.setDerniereDirection("S");
		this.moved = false;
		this.v = new Vie(5);
	}
	

	public Personnage(String name, int vie ){
		if(name == null)
			name = "Inconnu";
		this.nom = name;
		
		//initialisation de la direction a rester sur place
		this.direction=new int[2];
		this.direction[0]=0;
		this.direction[1]=0;
		this.setDerniereDirection("S");
		this.moved = false;
		this.v = new Vie(vie);
	}
	
	
	
		//METHODES
	
	/**
	 * Méthode change_direction
	 * Permet au personnage de changer la direction
	 * dans laquelle il avance
	 * @param dir	nouvelle direction
	 */
	public void change_direction(String dir){
		this.setDerniereDirection(dir.toUpperCase());
		switch(dir.toUpperCase()){
		
		case "N" :
			this.direction[0]=0;
			this.direction[1]=-1;
			break;
		case "S" :
			this.direction[0]=0;
			this.direction[1]=1;
			break;	
		case "E" :
			this.direction[0]=1;
			this.direction[1]=0;
			break;
		case "O" :
			this.direction[0]=-1;
			this.direction[1]=0;
			break;	
		default:
			this.direction[0]=0;
			this.direction[1]=0;
		}
	}

	/**
	 * Getter de moved
	 * @return moved
	 */
	public boolean isMoved() {
		return this.moved;
	}

	/**
	 * Setter de moved
	 * @param m
	 */
	public void setMoved(boolean m) {
		this.moved = m;
	}

	/**
	 * Getter de espace
	 * @return espace
	 */
	public Case getEspace() {
		return this.espace;
	}

	/**
	 * Setter de espace
	 * @param e
	 */
	public void setEspace(Case e) {
		this.espace = e;
	}
	
	/**
	 * Getter de l'abscisse du déplacement
	 * @return int
	 */
	public int getDeplacementX(){
		return this.direction[0];
	}
	
	/**
	 * Getter de l'ordonnée du déplacement
	 * @return int
	 */
	public int getDeplacementY(){
		return this.direction[1];
	}

	/**
	 * Getter du nom du personnage
	 * @return
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Setter du nom du personnage
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Getter du Tag du personnage
	 * @return
	 */
	public String getTag() {
		return TAG;
	}
	
	/**
	 * Getter de la vie du Personnage
	 * @return vie
	 */
	public Vie getVie()
	{
		return this.v;
	}
	
	/**
	 * Méthode estVivant
	 * Met le booleen à false si le perso est mort
	 */
	public void estVivant()
	{
		if(this.v.getValeur()<=0)
			this.vivant = false;
	}
	
	/**
	 * Getter de vivant
	 * @return vivant
	 */
	public boolean getVivant()
	{
		return this.vivant;
	}

	public String getDerniereDirection() {
		return derniereDirection;
	}

	public void setDerniereDirection(String derniereDirection) {
		this.derniereDirection = derniereDirection;
	}
	

	
	
}