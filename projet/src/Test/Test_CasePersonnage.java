package Test;

import static org.junit.Assert.*;
import org.junit.*;
import Case.*;
import Personnage.*;

/**
 * Classe permettant de tester la classe Case du package Case
 * @author FREI LAMBOLEZ LOGEART PERES
 *
 */
public class Test_CasePersonnage {

	private Personnage p;
	
	@Before
	public void initialize(){
		p = new Personnage ("Champion");
	}

	
	@Test
	public void test_constructeur() {
		Case_Personnage c = new Case_Personnage (5, 8, p);
		assertEquals ("L'abscisse devrait etre a 5", 5, c.getPosX());
		assertEquals ("L'ordonnée devrait etre a 8", 8, c.getPosY());
		assertEquals ("Le Personnage existe ", p, c.getPersonnage());
		assertEquals ("Le nom de la case doit etre le tag du personnage", "P", c.getPersonnage().getTag());
	}

	@Test
	public void test_constructeur_mauvaisX(){
		Case_Personnage c = new Case_Personnage (-3, 8, p);
		assertEquals ("L'abscisse devrait etre a 0", 0, c.getPosX());
		assertEquals ("L'ordonnée devrait etre a 8", 8, c.getPosY());
		assertEquals ("Le Personnage existe ", p, c.getPersonnage());
		assertEquals ("Le nom de la case doit etre le tag du personnage", "P", c.getPersonnage().getTag());
	}
	
	@Test
	public void test_constructeur_mauvaisY(){
		Case_Personnage c = new Case_Personnage (5, -2, p);
		assertEquals ("L'abscisse devrait etre a 5", 5, c.getPosX());
		assertEquals ("L'ordonnée devrait etre a 0", 0, c.getPosY());
		assertEquals ("Le Personnage existe ", p, c.getPersonnage());
		assertEquals ("Le nom de la case doit etre le tag du personnage", "P", c.getPersonnage().getTag());
	}
	
	@Test
	public void test_constructeur_mauvaisPers(){
		Case_Personnage c = new Case_Personnage (5, 8, p);
		assertEquals ("L'abscisse devrait etre a 5", 5, c.getPosX());
		assertEquals ("L'ordonnée devrait etre a 8", 8, c.getPosY());
		assertEquals ("Le Personnage existe ", p, c.getPersonnage());
		assertEquals ("Le nom de la case doit etre le tag du personnage", "P", c.getPersonnage().getTag());
	}
}
