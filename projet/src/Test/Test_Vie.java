package Test;

import static org.junit.Assert.*;
import org.junit.Test;
import Personnage.*;
/**
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Test_Vie
 * Permet de tester la classe Vie
 * Test V.2.1
 */
public class Test_Vie {

	
	@Test
	public void test_constructeurVide(){
		Vie v = new Vie();
		assertEquals("La vie devrait être à 5", 5, v.getValeur());
	}
	
	@Test
	public void test_constructeur1(){
		Vie v = new Vie(8);
		assertEquals("La vie devrait être à 8", 8, v.getValeur());
	}
	
	@Test
	public void test_constructeur1MauvaiseValeur(){
		Vie v = new Vie(-1);
		assertEquals("La vie devrait être mise à 5",5,v.getValeur());
	}
	
	@Test
	public void test_incrementer(){
		Vie v = new Vie();
		assertEquals("La vie devrait être à 5", 5, v.getValeur());
		v.incrementer();
		assertEquals("La vie devrait passer à 6", 6, v.getValeur());
	}
	
	@Test
	public void test_decrementer(){
		Vie v = new Vie();
		assertEquals("La vie devrait être à 5", 5, v.getValeur());
		v.decrementer();
		assertEquals("La vie devrait descendre à 4", 4, v.getValeur());
	}
	
	@Test
	public void test_decrementerNegatif(){
		Vie v = new Vie(1);
		assertEquals("La vie devrait être à 1", 1, v.getValeur());
		v.decrementer();
		assertEquals("La vie passe à 0", 0, v.getValeur());
		v.decrementer();
		assertEquals("La vie ne peut pas être négative", 0, v.getValeur());
	}

}