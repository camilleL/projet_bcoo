package Test;
import static org.junit.Assert.*;

import org.junit.Test;

import Personnage.*;
/**
 * 
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Test_Aventurier
 * Permet de tester la classe Aventurier
 */
public class Test_Aventurier {

	@Test
	public void test_1_Constructeur(){
		Aventurier av = new Aventurier("Jones");
		assertEquals ("Le nom de l'aventurier doit être Jones", "Jones", av.getNom());
		assertEquals("L'aventurier a une vie de 5 par défaut", 5, av.getVie().getValeur());		//Ajout version 2.1
		assertEquals("L'aventurier ne se déplace pas", 0, av.getDeplacementX());
		assertEquals("L'aventurier ne se déplace pas", 0, av.getDeplacementY());
		assertTrue(!av.isMoved());
	}
	
	@Test
	public void test_2_Constructeur_MauvaisNom(){
		Aventurier av = new Aventurier(null);
		assertEquals("Le nom est inconnu", "Inconnu", av.getNom());
		assertEquals("L'aventurier a une vie de 5 par défaut", 5, av.getVie().getValeur());		//Ajout version 2.1
		assertEquals("L'aventurier ne se déplace pas", 0, av.getDeplacementX());
		assertEquals("L'aventurier ne se déplace pas", 0, av.getDeplacementY());
		assertTrue(!av.isMoved());
	}
	
	@Test
	public void test_3_Tag(){
		Aventurier av = new Aventurier("Jones");
		assertEquals("Le tag de l'aventurier doit être PJ", "PJ", av.getTag());
	}

	@Test
	public void test_4_ChangeDirection(){
		Aventurier av = new Aventurier("Jones");
		assertEquals ("Le nom de l'aventurier doit être Jones", "Jones", av.getNom());
		assertEquals("L'aventurier a une vie de 5 par défaut", 5, av.getVie().getValeur());		//Ajout version 2.1
		assertEquals("L'aventurier ne se déplace pas", 0, av.getDeplacementX());
		assertEquals("L'aventurier ne se déplace pas", 0, av.getDeplacementY());
		assertTrue(!av.isMoved());
		
		av.change_direction("N");
		assertEquals("L'aventurier est allé au Nord", 0, av.getDeplacementX());
		assertEquals("L'aventurier est allé au Nord, l'ordonnée est 'monté' ", -1, av.getDeplacementY());
		
		av.change_direction("S");
		assertEquals("L'aventurier est allé au Sud ", 0, av.getDeplacementX());
		assertEquals("L'aventurier est allé au Sud, l'ordonnée est 'descendu' ", 1, av.getDeplacementY());
		
		av.change_direction("O");
		assertEquals("L'aventurier est allé a l'ouest, l'abscisse se deplace à gauche ", -1, av.getDeplacementX());
		assertEquals("L'aventurirer est allé a l'ouest", 0, av.getDeplacementY());
		
		av.change_direction("E");
		assertEquals("L'aventurier est allé a l'ouest, l'abscisse se deplace à droite ", 1, av.getDeplacementX());
		assertEquals("L'aventurier est allé a l'ouest", 0, av.getDeplacementY());
	}
	
}