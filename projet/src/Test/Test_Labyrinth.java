package Test;

import static org.junit.Assert.*;

import org.junit.*;

import Case.*;
import main.*;
import Personnage.*;

public class Test_Labyrinth {

	private Labyrinth lab;
	
	@Before
	public void initialize(){
		lab = new Labyrinth (20,20);
	}
	 
	
	@Test
	public void test_Constructeur() {
		assertEquals("La taille du labyrinth est de 20 par 20 ", 20, lab.getTabCase().length);
		assertEquals("La taille du labyrinth est de 20 par 20 ", 20, lab.getTabCase()[0].length);
		
	}
	
	
	 @Test
	 public void test_CheckCase(){
	  int val = lab.checkCase(1, 1);
	  assertTrue("La case doit être vide", val==0);
	  val = lab.checkCase(19, 18);
	  assertTrue("La case doit être vide", val==0);
	 }

	
	@Test
	public void test_addPerso_MauvaisX(){
		Personnage p = new Personnage ("Dieu");
		lab.addPerso(100, 10, p);
		assertTrue("Aucun personnage n'est ajouté",lab.checkCase(10, 10)==0);
	}
	
	@Test
	public void test_addPerso_MauvaisY(){
		Personnage p = new Personnage ("Dieu");
		lab.addPerso(10, -10, p);
		assertTrue("Aucun personnage n'est ajouté",lab.checkCase(10, 10)==0);
	}
	
	@Test
	public void test_addPerso(){
		Personnage p = new Personnage ("Dieu");
		assertTrue("Case vide", lab.checkCase(10, 10)==0);
		lab.addPerso(10, 10, p);
		assertTrue("Personnage ajouté",lab.checkCase(10, 10)!=0);
	}

	@Test
	public void test_addMur(){
		assertTrue("Case vide", lab.checkCase(10, 10)==0);
		lab.addMur(10, 10);
		assertTrue("case mur ajouté",lab.checkCase(10, 10)!=0);
	}
	
	@Test
	public void test_addMur_MauvaisX(){
		lab.addMur(-10, 10);
		assertTrue("case mur ajouté",lab.checkCase(10, 10)==0);
	}
	
	
	@Test
	public void test_addMur_MauvaisY(){
		lab.addMur(10, 100);
		assertTrue("case mur ajouté",lab.checkCase(10, 10)==0);
	}
	
	
	@Test
	 public void test_Deplacer(){
	  boolean deplace;
	  Personnage p = new Personnage ("Dieu");
	  assertTrue("case vide au départ",lab.checkCase(10, 10)==0);
	  lab.addPerso(10, 10, p);
	  assertTrue("Personnage ajouté",lab.checkCase(10, 10)!=0);
	  
	  
	  p.change_direction("N");
	  int x = lab.getTabCase()[10][10].getPosX() + p.getDeplacementX();
	  int y = lab.getTabCase()[10][10].getPosY() + p.getDeplacementY();
	  
	  deplace = lab.deplacer(lab.getTabCase()[10][10]);
	  assertTrue ("Le personnage a changé de case" , lab.checkCase(x, y)!=0 );
	  assertTrue("case vide, personnage n'est plus la", lab.checkCase(10, 10)==0);
	 }
}
