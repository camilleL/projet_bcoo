package Test;

import Case.*;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Classe permettant de tester la classe Case du package Case
 * @author FREI LAMBOLEZ LOGEART PERES
 *
 */
public class Test_Case {

	

	@Test
	public void test_constructeur() {
		Case c = new Case (5, 8, "Jolie Case");
		assertEquals ("L'abscisse devrait etre a 5", 5, c.getPosX());
		assertEquals ("L'ordonnée devrait etre a 8", 8, c.getPosY());
		assertEquals ("Le nom devrait etre 'Jolie Case' ", "Jolie Case", c.getNom());
	}

	@Test
	public void test_constructeur_mauvaisX(){
		Case c = new Case (-3, 8, "Case");
		assertEquals ("L'abscisse devrait etre a 0", 0, c.getPosX());
		assertEquals ("L'ordonnée devrait etre a 8", 8, c.getPosY());
		assertEquals ("Le nom devrait etre 'Case' ", "Case", c.getNom());
	}
	
	@Test
	public void test_constructeur_mauvaisY(){
		Case c = new Case (5, -2, "Case");
		assertEquals ("L'abscisse devrait etre a 5", 5, c.getPosX());
		assertEquals ("L'ordonnée devrait etre a 0", 0, c.getPosY());
		assertEquals ("Le nom devrait etre 'Case' ", "Case", c.getNom());
	}
	
	@Test
	public void test_constructeur_mauvaisNom(){
		Case c = new Case (5, 8, null);
		assertEquals ("L'abscisse devrait etre a 5", 5, c.getPosX());
		assertEquals ("L'ordonnée devrait etre a 8", 8, c.getPosY());
		assertEquals ("Le nom devrait etre _ par défaut ", "_", c.getNom());
	}
}
