package Test;
import Case.*;
import static org.junit.Assert.*;
import org.junit.*;

/**
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Test_Case_Mur
 * Permet de tester la classe Case_Mur
 */
public class Test_CaseMur {

	@Test
	public void test_1_Constructeur(){
		Case_Mur cm = new Case_Mur(1, 3);
		assertEquals("L'abscisse est normalement de 1", 1, cm.getPosX());
		assertEquals("L'ordonnée est normalement de 3", 3, cm.getPosY());
		assertEquals("Le nom est MUR par défaut", "MUR", cm.getNom());
	}
	
	@Test
	public void test_2_YNegatif(){
		Case_Mur cm = new Case_Mur(1, -5);
		assertEquals("L'abscisse est normalement de 1", 1, cm.getPosX());
		assertEquals("L'ordonnée doit être mise à 0", 0, cm.getPosY());
		assertEquals("Le nom est MUR par défaut", "MUR", cm.getNom());
	}
	
	@Test
	public void test_3_XNegatif(){
		Case_Mur cm = new Case_Mur(-5, 3);
		assertEquals("L'abscisse doit être mise à 0", 0, cm.getPosX());
		assertEquals("L'ordonnée est normalement de 3", 3, cm.getPosY());
		assertEquals("Le nom est MUR par défaut", "MUR", cm.getNom());
	}
}