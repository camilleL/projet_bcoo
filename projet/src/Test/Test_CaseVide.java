package Test;
import Case.*;
import static org.junit.Assert.*;
import org.junit.*;
/**
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Test_Case_Mur
 * Permet de tester la classe Case_Mur
 */
public class Test_CaseVide {

	@Test
	public void test_1_Constructeur(){
		Case_vide cv = new Case_vide(1, 3);
		assertEquals("L'abscisse est normalement de 1", 1, cv.getPosX());
		assertEquals("L'ordonnée est normalement de 3", 3, cv.getPosY());
		assertEquals("Le nom est _ par défaut", "_", cv.getNom());
	}
	
	@Test
	public void test_2_YNegatif(){
		Case_vide cv = new Case_vide(1, -5);
		assertEquals("L'abscisse est normalement de 1", 1, cv.getPosX());
		assertEquals("L'ordonnée doit être mise à 0", 0, cv.getPosY());
		assertEquals("Le nom est _ par défaut", "_", cv.getNom());
	}
	
	@Test
	public void test_3_XNegatif(){
		Case_vide cv = new Case_vide(-5, 3);
		assertEquals("L'abscisse doit être mise à 0", 0, cv.getPosX());
		assertEquals("L'ordonnée est normalement de 3", 3, cv.getPosY());
		assertEquals("Le nom est _ par défaut", "_", cv.getNom());
	}
}