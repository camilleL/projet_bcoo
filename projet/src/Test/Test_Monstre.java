package Test;

import static org.junit.Assert.*;
import Personnage.*;

import org.junit.Test;
/**
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Test_Monstre
 * Permet de tester la classe Monstre
 * Test V.2.1
 */
public class Test_Monstre {

	
	@Test
	public void test_constructeur(){
		Monstre m = new Monstre("Dracula");
		assertEquals ("Le monstre a le nom Dracula", "Dracula", m.getNom());
		assertEquals("Le monstre a une vie de 5", 5, m.getVie().getValeur());
		assertEquals("Le monstre ne se déplace pas", 0, m.getDeplacementX());
		assertEquals("Le monstre ne se déplace pas", 0, m.getDeplacementY());
		assertTrue(!m.isMoved());
	}
	
	@Test
	public void test_tag(){
		Monstre m = new Monstre("Dracula");
		assertEquals("Le tag du monstre doit être M", "M", m.getTag());
	}
	
	@Test
	public void test_constructeurMauvaisNom(){
		Monstre m = new Monstre(null);
		assertEquals("Le nom du monstre est inconnu", "Inconnu", m.getNom());
		assertEquals("Le monstre a une vie de 5", 5, m.getVie().getValeur());
		assertEquals("Le monstre ne se déplace pas", 0, m.getDeplacementX());
		assertEquals("Le monstre ne se déplace pas", 0, m.getDeplacementY());
		assertTrue(!m.isMoved());
	}
	
	@Test
	public void test_changerDirection(){
		Monstre m = new Monstre("Dracula");
		
		m.change_direction("N");
		assertEquals("Le monstre est allé au Nord", 0, m.getDeplacementX());
		assertEquals("Le monstre est allé au Nord, l'ordonnée est 'monté' ", -1, m.getDeplacementY());
		
		m.change_direction("S");
		assertEquals("Le monstre est allé au Sud ", 0, m.getDeplacementX());
		assertEquals("Le monstre est allé au Sud, l'ordonnée est 'descendu' ", 1, m.getDeplacementY());
		
		m.change_direction("O");
		assertEquals("Le monstre est allé a l'ouest, l'abscisse se deplace à gauche ", -1, m.getDeplacementX());
		assertEquals("Le monstre est allé a l'ouest", 0, m.getDeplacementY());
		
		m.change_direction("E");
		assertEquals("Le monstre est allé a l'ouest, l'abscisse se deplace à droite ", 1, m.getDeplacementX());
		assertEquals("Le monstre est allé a l'ouest", 0, m.getDeplacementY());
	}

}