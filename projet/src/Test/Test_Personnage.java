package Test;

import static org.junit.Assert.*;
import org.junit.*;
import Personnage.*;
/**
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Test_Personnage
 * Permet de tester la classe Personnage
 */
public class Test_Personnage {

	

	@Test
	public void test_ConstructeurVide() {
		Personnage p = new Personnage ();
		assertEquals("Le nom du personnage dervait etre 'Inconnu'", "Inconnu", p.getNom());
		assertEquals("Le personnage a une vie de 5 par défaut", 5, p.getVie().getValeur());		//Ajout version 2.1
		assertEquals("Le personnage ne se déplace pas ", 0, p.getDeplacementX());
		assertEquals("Le personnage ne se deplace pas ", 0, p.getDeplacementY());
		assertTrue(!p.isMoved());
	}
	
	@Test
	public void test_ConstructeurParam(){
		Personnage p = new Personnage ("Hero");
		assertEquals("Le nom du personnage devrait etre 'Hero'", "Hero", p.getNom());
		assertEquals("Le personnage a une vie de 5 par défaut", 5, p.getVie().getValeur());		//Ajout version 2.1
		assertEquals("Le personnage ne se déplace pas ", 0, p.getDeplacementX());
		assertEquals("Le personnage ne se deplace pas ", 0, p.getDeplacementY());
		assertTrue(!p.isMoved());
	}

	@Test
	public void test_ConstructeurMauvaisParam(){
		Personnage p = new Personnage (null);
		assertEquals("Le nom du personnage devrait etre 'Inconnu'", "Inconnu", p.getNom());
		assertEquals("Le personnage a une vie de 5 par défaut", 5, p.getVie().getValeur());		//Ajout version 2.1
		assertEquals("Le personnage ne se déplace pas ", 0, p.getDeplacementX());
		assertEquals("Le personnage ne se deplace pas ", 0, p.getDeplacementY());
		assertTrue(!p.isMoved());
	}
	
	@Test
	public void test_ChangeDirection(){
		Personnage p = new Personnage ();
		assertEquals("Le personnage est placé par défaut ", 0, p.getDeplacementX());
		assertEquals("Le personnage est placé par défaut ", 0, p.getDeplacementY());
		assertEquals("Le personnage a une vie de 5 par défaut", 5, p.getVie().getValeur());		//Ajout version 2.1
		
		p.change_direction("N");
		assertEquals("Le personnage est allé au Nord ", 0, p.getDeplacementX());
		assertEquals("Le personnage est allé au Nord, l'ordonnée est 'monté' ", -1, p.getDeplacementY());
		
		p.change_direction("S");
		assertEquals("Le personnage est allé au Sud ", 0, p.getDeplacementX());
		assertEquals("Le personnage est allé au Sud, l'ordonnée est 'descendu' ", 1, p.getDeplacementY());
		
		p.change_direction("O");
		assertEquals("Le personnage est allé a l'ouest, l'abscisse se deplace à gauche ", -1, p.getDeplacementX());
		assertEquals("Le personnage est allé a l'ouest", 0, p.getDeplacementY());
		
		p.change_direction("E");
		assertEquals("Le personnage est allé a l'ouest, l'abscisse se deplace à droite ", 1, p.getDeplacementX());
		assertEquals("Le personnage est allé a l'ouest", 0, p.getDeplacementY());
	}
	
	//Nouveaux tests V.2.1.
	
	@Test
	public void test_constructeurVie(){
		Personnage p = new Personnage("Hero", 5);
		assertEquals("Le personnage porte le nom 'Hero'", "Hero", p.getNom());
		assertEquals("Le personnage a une vie de 5", 5, p.getVie().getValeur());
		assertEquals("Le personnage ne se déplace pas ", 0, p.getDeplacementX());
		assertEquals("Le personnage ne se deplace pas ", 0, p.getDeplacementY());
		assertTrue(!p.isMoved());
	}
	
	@Test
	public void test_estVivant(){
		Personnage p = new Personnage("Hero", 1);
		assertEquals("Le personnage a une vie de 1", 1, p.getVie().getValeur());
		assertTrue("Le personnage est en vie",p.getVivant());
	}
}