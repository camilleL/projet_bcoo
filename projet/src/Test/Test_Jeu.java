package Test;

import static org.junit.Assert.*;
import org.junit.Test;
import main.*;
import Personnage.*;

public class Test_Jeu {

	
	@Test
	public void test_constructeur() {
		Jeu jeu = new Jeu ("Marsu");
		assertEquals ("L'aventurier devrait s'appeler 'Marsu'", "Marsu", jeu.getJoueur().getNom());
		assertEquals( "Le Labirynthe est par défaut 20, 20", 20, jeu.getLab().getTabCase().length );
		assertEquals( "Le Labirynthe est par défaut 20, 20", 20, jeu.getLab().getTabCase()[0].length );
	}

	
}
