package Case;

import Personnage.Personnage;

/**
 * Modélise une case à évènement
 *
 */



public class Case_Event extends Case
{

	
	/** Attribut stockant un personnage */
	private Personnage personnage;
	/** Attribut stockant un objet (String)*/
	private String objet ;
	
	public Case_Event(int x, int y, String n, String o) 
	{
		super(x, y, n);
		this.personnage = null;
		this.objet = o;
	}
	
	/**
	 * Getter objet
	 * @return l'objet de la case_event
	 */
	public String getObjet()
	{
		return this.objet;
	}
	
	/**
	 * Setter de l'objet
	 * @param o
	 */
	public void setObjet(String o)
	{
		this.objet = o;
	}
	
	/**
	 * Getter de personnage
	 * @return le personnage présent sur la case courante
	 */
	public Personnage getPersonnage(){
		return this.personnage;
	}
	
	/**
	 * Modifie le personnage présent sur la case courante
	 * @param p, le nouveau personnage qui occupera la case
	 */
	public void setPersonnage(Personnage p){
		if (p != null)
			this.personnage = p;
	}
	
	
	
}
