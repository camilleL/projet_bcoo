package Case;
/**
 * 
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Case_Mur
 * Modélise une case qui contient un mur
 */
public class Case_Mur extends Case{

	//CONSTRUCTEUR
	
	/**
	 * Constructeur à 2 paramètres
	 * @param x		abscisse donnée
	 * @param y		ordonnée donnée
	 */
	public Case_Mur(int x,int y){
		super(x, y, "MUR");
		}
}
