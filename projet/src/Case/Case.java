package Case;
/**
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Case
 * Représente une case dans le labyrinthe
 */
public class Case {
	
		//ATTRIBUTS
	
	/** Nom de la case*/
	private String nom;
	/** Abscisse de la case*/
	private int posX;
	/** Ordonnée de la case*/
	private int posY;
	
	
		//METHODES
	
	public Case(int x,int y,String n){
		if(n == null)
			n = "_";	
		if(x < 0)
			x = 0;
		if(y < 0)
			y= 0;
		this.posX=x;
		this.posY=y;
		this.nom=n;
	}
	
	
	/**
	 * Getter du nom de la casee
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Setter du nom de la case
	 * @param nom
	 */
	public void setNom(String n) {
		this.nom = n;
	}

	/**
	 * Getter de x
	 * @return
	 */
	public int getPosX() {
		return posX;
	}

	/**
	 * Setter de x
	 * @param posX
	 */
	public void setPosX(int posX) {
		this.posX = posX;
	}

	/**
	 * Getter de y
	 * @return
	 */
	public int getPosY() {
		return posY;
	}

	/**
	 * Setter de y
	 * @param posY
	 */
	public void setPosY(int posY) {
		this.posY = posY;
	}

	public String toString(){
		return this.nom;
	}
}
