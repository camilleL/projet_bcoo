package Case;
import Personnage.*;

/**
 * @author FREI  LAMBOLEZ  LOGEART  PERES
 * Classe Case_Personnage
 * Modélise une case contenant un personnage
 */
public class Case_Personnage extends Case{
	
			// ATTRIBUTS
	
	
	/** Personnage present sur la case */
	private Personnage personnage;
	
	
	
			// CONSTRUCTEUR
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param p
	 */
	public Case_Personnage(int x, int y, Personnage p){
		super(x, y, p.getTag());

		if (p == null){
			p = new Personnage ();
		}
		this.personnage = p;
		
		p.setEspace(this);
	
		
	}
	
	
	
			// METHODES
	
	
	/**
	 * Getter de personnage
	 * @return le personnage présent sur la case courante
	 */
	public Personnage getPersonnage(){
		return this.personnage;
	}
	
	/**
	 * Modifie le personnage présent sur la case courante
	 * @param p, le nouveau personnage qui occupera la case
	 */
	public void setPersonnage(Personnage p){
		if (p != null)
			this.personnage = p;
	}
	
	
}
