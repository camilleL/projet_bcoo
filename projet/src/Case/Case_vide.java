package Case;
/**
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Case_vide
 * Hérite de Case
 * Représente une case vide dans le labyrinthe
 */
public class Case_vide extends Case {

	// CONSTRUCTEUR
	
	/**
	 * Constructeur à 2 paramètres
	 * @param x		abscisse donnée
	 * @param y		ordonnée donnée
	 */
	public Case_vide(int x,int y){
		super(x, y, "_");
	}
	
	
}
