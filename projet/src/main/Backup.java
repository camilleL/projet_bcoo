package main;

import java.io.*;
import java.util.*;

import Case.*;
import Personnage.Aventurier;
import Personnage.Monstre;

public class Backup {

	String adress;
	
	
	public Backup(String adr){
		if(adr==""){
			adr="level/default.txt";
		}
		this.adress=adr;
	}
	
	public Jeu load(){
		Jeu charger=null;
		try{
			BufferedReader br = new BufferedReader(new FileReader(this.adress));
			
			String lu = br.readLine();
			
			String[] decouper =lu.split(";");
			
			charger = new Jeu();
			int vie = Integer.parseInt(decouper[1]);
			
			System.out.println("Nom de votre personnage ?");
			Scanner sc = new Scanner(System.in);
			String name = sc.nextLine();
			
			Aventurier player = new Aventurier(name,vie);
			
			
			int hauteur = Integer.parseInt(decouper[2]);
			int largeur = Integer.parseInt(decouper[3]);
			
			Labyrinth lab = new Labyrinth(hauteur, largeur);
			
			int y=0;
			
			while((lu=br.readLine())!=null){
				decouper =lu.split(" ");
				for(int x=0;x<largeur;x++){
					switch(decouper[x]){
					case "1":
						lab.addMur(x, y);
						break;
					case "2":
						lab.addPerso(x, y, player);
						charger.setJoueur(player);
						break;
					case "3":
						lab.addPerso(x, y,new Monstre("Edgar"));
					case "4" :
						lab.addEvent(x, y, "E", "Amulette");
					case "5" :
						lab.addEvent(x, y, "E", "Piege");
					case "6" :
						lab.addEvent(x,y, "W", "win");
					case "7" :
						lab.addEvent(x, y, "E", "pizza");
					}
				}
				y++;
			}
			charger.setLab(lab);
			br.close();
		}catch(FileNotFoundException e1){
			System.out.println("Fichier "+this.adress+"introuvable");
		}catch (IOException e2) {
			e2.printStackTrace();
		}
		return charger;
	}
	
	public void save(Jeu toSave){
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter(this.adress));
			String nom = toSave.getJoueur().getNom();
			double vie = toSave.getJoueur().getVie().getValeur();
			int tabX = toSave.getLab().getTabCase().length;
			int tabY = toSave.getLab().getTabCase()[0].length;
			String str=nom+";"+vie+";"+tabX+";"+tabY+";";
			bw.write(str+"\n");
			int val;
			for(int y=0;y<toSave.getLab().getTabCase()[0].length;y++){
				str = "";
				for(int x=0;x<toSave.getLab().getTabCase().length;x++){
					switch(toSave.getLab().getTabCase()[x][y].toString()){
					case "_":
						val=0;
						break;
					case "MUR":
						val=1;
						break;
					case "PJ":
						val=2;
						break;
					case "M":
						val=3;
						break;
					default:
						val=0;
					}
					str += " "+val;
				}
				bw.write(str+"\n");
			}
			bw.close();
			
			
		}catch(FileNotFoundException e1){
			System.out.println("Fichier "+this.adress+"introuvable");
		}catch (IOException e2) {
			e2.printStackTrace();
		}
		
		
		
	}
}
