package main;

import javax.swing.JOptionPane;

import Case.*;
import Personnage.*;
import Vue.moteurGraphique.gui.InterfaceGraphique;

/**
 * @author FREI LAMBOLEZ LOGEART PERES
 * Classe Labyrinth
 * Représente le labyrinthe dans lequel évolue le personnage
 */
public class Labyrinth {
	
		//ATTRIBUT
	
	/** Attribut créant un tableau bidimensionnel de Case*/
	private Case[][] tabCase ;
	
	
		//CONSTRUCTEUR
	
	/**
	 * Constructeur de Labyrinth
	 * @param x, int pour la taille de la première dimension
	 * @param y, int pour la taille de la deuxième dimension
	 */
	public Labyrinth(int x, int y)
	{
		this.tabCase = new Case[x][y];
		for(int i = 0; i < this.tabCase.length ; i++)
		{
			for(int j = 0; j < this.tabCase[i].length ; j++)
			{
				this.tabCase[i][j]=new Case_vide(i,j);
			}
		}
					
				
	}
	
	/**
	 * Méthode evoluer
	 * permettant d'évoluer dans le labyrinthe
	 * en fonction des déplacements 
	 */
	public void evoluer(){
		for(int i = 0; i < this.tabCase.length ; i++){
			for(int j = 0; j < this.tabCase[i].length ; j++){

				if(this.tabCase[i][j] instanceof Case_Personnage){
					if(!(((Case_Personnage) this.tabCase[i][j]).getPersonnage().isMoved())){
						((Case_Personnage) this.tabCase[i][j]).getPersonnage().setMoved(true);
						this.deplacer(this.tabCase[i][j]);
					}
				}

			}
		}

		for(int i = 0; i < this.tabCase.length ; i++){
			for(int j = 0; j < this.tabCase[i].length ; j++){
				if(this.tabCase[i][j] instanceof Case_Personnage){
					((Case_Personnage) this.tabCase[i][j]).getPersonnage().setMoved(false);
					((Case_Personnage) this.tabCase[i][j]).getPersonnage().estVivant();
					if(((Case_Personnage) this.tabCase[i][j]).getPersonnage().getVivant()==false)
					{
						System.out.println("Le personnage "+((Case_Personnage) this.tabCase[i][j]).getPersonnage().getNom()+" est mort");
						if(((Case_Personnage) this.tabCase[i][j]).getPersonnage().getTag().equals("M")){
							int x = (int)(Math.random() *10);
							int y = (int)(Math.random() *10);
							while (!(this.checkCase(x, y) == 0) || (x> this.tabCase.length) || (y>this.tabCase.length)){
								x = (int)(Math.random() *10);
								y = (int)(Math.random() *10);
							}
							this.addPerso(x, y, new Monstre("Bobby"));
						}

						
						if(((Case_Personnage) this.tabCase[i][j]).getPersonnage().getTag().equals("PJ")){
							System.out.println("Vous avez perdu la partie !");
							System.exit(0);
						}
						this.tabCase[i][j]= new Case_vide(i, j);
					}
				}
			}
		}
	}


	
	/**
	 * Méthode checkCase
	 * Vérifie si la case est vide
	 * @param x		abscisse de l'emplacement de la case
	 * @param y		ordonnée de l'emplacement de la case
	 * @return	int
	 * 0 seulement si la case est vide
	 */
	public int checkCase(int x, int y) {
		int val;
		
		
		switch(this.tabCase[x][y].toString()){
		case "_":
			val=0;
			break;
		case "MUR":
			val=1;
			break;
		case "PJ":
			val=2;
			break;
		case "M":
			val=3;
			break; 
		case "E" :
			val = 4 ;
			break;
		case "W" :
			val = 6 ;
			break;
		default:
			val=-1;
		}
		
		if((x < 0 || x >= this.tabCase.length) || (y < 0 || y >= this.tabCase[0].length))
			val = -1;

		return val;
	}

	/**
	 * Méthode addPerso
	 * Vérifie si on peut ajouter le personnage dans la case
	 * @param x		abscisse de l'emplacement de la case
	 * @param y		ordonnée de l'emplacement de la case
	 * @param p		personnage à ajouter
	 * @return	boolean
	 * true seulement si l'ajout est possible
	 */
	public boolean addPerso(int x, int y, Personnage p){
		boolean ajout = false;
		if( (x < this.tabCase.length) && ( x>= 0) && (y < this.tabCase[0].length) && (y>=0) ) {
			
			if(this.checkCase(x, y)==0){
				this.tabCase[x][y] = new Case_Personnage(x, y, p); 
				// ajouter p à la case
				ajout = true;
			}
		}
		return ajout ;
	}
	
	/**
	 * Méthode addMur
	 * Vérifie si on peut ajouter un mur dans la case
	 * @param x		abscisse de l'emplacement de la case
	 * @param y		ordonnée de l'emplacement de la case
	 * @return	boolean
	 * true seulement si l'ajout est possible
	 */
	public boolean addMur(int x, int y){
		boolean ajout = false;
		if( (x < this.tabCase.length) && ( x>= 0) && (y < this.tabCase[0].length) && (y>=0) )  {
			if(this.checkCase(x, y)==0){
				this.tabCase[x][y] = new Case_Mur(x, y); 
				ajout = true;
			}
		}
		return ajout ;
	}
	
	public boolean addEvent(int x, int y, String n, String o)
	{
		boolean ajout = false;
		if( (x < this.tabCase.length) && ( x>= 0) && (y < this.tabCase[0].length) && (y>=0) )  {
			if(this.checkCase(x, y)==0){
				this.tabCase[x][y] = new Case_Event(x, y,n,o); 
				ajout = true;
			}
		}
		return ajout ;
		
	}
	
	/**
	 * Méthode deplacer
	 * Vérifie si le personage peut se déplacer
	 * @param c		case dans laquelle va le personnage
	 * @return	boolean
	 * true seulement si le déplacement est possible
	 */
	public boolean deplacer(Case c){
		boolean dep = false;
		int x = c.getPosX() + ((Personnage)((Case_Personnage) c).getPersonnage()).getDeplacementX();
		int y = c.getPosY() + ((Personnage)((Case_Personnage) c).getPersonnage()).getDeplacementY();
		//System.out.println(this.checkCase(c.getPosX(),c.getPosY()));
		if(this.checkCase(x, y)==0) {
			dep = true;
			this.tabCase[c.getPosX()][c.getPosY()]= new Case_vide(c.getPosX(), c.getPosY());
			this.addPerso(x, y, ((Case_Personnage)c).getPersonnage() );
			
		}else if(this.checkCase(x, y)==1)
		{
			if(this.checkCase(c.getPosX(),c.getPosY())==2){
				//On touche un mur
			}
		}
		else if(this.checkCase(x, y)==4)
		{
			dep = true;
			
			if(((Case_Personnage)c).getPersonnage().getTag()=="PJ")
			{
				switch(((Case_Event) this.tabCase[x][y]).getObjet())
				{
				case "Amulette" :
					((Aventurier)((Case_Personnage) this.tabCase[c.getPosX()][c.getPosY()]).getPersonnage()).setAmulette(true);
					System.out.println("Vous avez trouvé l'amulette");
					break;
				case "Piege" :
					((Case_Personnage) tabCase[c.getPosX()][c.getPosY()]).getPersonnage().getVie().decrementer();
					System.out.println("Vous êtes blessé par un piège");
					
					break;
				/*
				case "win" :
					if(((Aventurier) ((Case_Personnage) tabCase[c.getPosX()][c.getPosY()]).getPersonnage()).getAmulette()==true)
					{
						System.out.println("Vous avez gagné !");
						System.exit(0);
					}*/
				case "pizza" :
				{
					((Case_Personnage) tabCase[c.getPosX()][c.getPosY()]).getPersonnage().getVie().incrementer();
					System.out.println("Vous avez trouvé une pizza, vous gagnez un coeur");
				}
				default :
					break;
					
					
				}
			
				this.tabCase[x][y] = new Case_vide(x,y);
				this.addPerso(x, y, ((Case_Personnage)c).getPersonnage() );
				this.tabCase[c.getPosX()][c.getPosY()]= new Case_vide(c.getPosX(), c.getPosY());
				
			}
			else if(((Case_Personnage)c).getPersonnage().getTag()=="M")
			{
				System.out.println(("Les monstres ne peuvent pas utiliser les coffres"));
			}
		}
		else if(this.checkCase(x, y)==6)
		{
			dep = false;
			
			if(((Case_Personnage)c).getPersonnage().getTag()=="PJ")
			{
				if(((Aventurier) ((Case_Personnage) tabCase[c.getPosX()][c.getPosY()]).getPersonnage()).getAmulette()==true)
				{
					System.out.println("Vous avez gagné !");
					System.exit(0);
				}else{
					System.out.println("Vous n'avez pas l'amulette pour sortir !");
				}
			}
			else if(((Case_Personnage)c).getPersonnage().getTag()=="M")
			{
				System.out.println(("Les monstres ne peuvent pas utiliser les sorties"));
			}
		}
		else if( (this.checkCase(x, y)!=this.checkCase(c.getPosX(),c.getPosY())))
		{
			
			System.out.println("Vous perdez un point de vie");
			
			if(this.checkCase(c.getPosX(),c.getPosY())==2){
				((Case_Personnage)tabCase[c.getPosX()][c.getPosY()]).getPersonnage().getVie().decrementer();
			}else{
				((Case_Personnage)tabCase[x][y]).getPersonnage().getVie().decrementer();
			}
						
			//this.tabCase[c.getPosX()][c.getPosY()]= new Case_vide(c.getPosX(), c.getPosY());
			
			//this.addPerso(c.getPosX(), c.getPosY(), ((Case_Personnage)c).getPersonnage() );
		}		
		
		return dep;
	}
	
	
	/**
	 * Methode permettant de déplacer le monstre
	 */
	public void deplacerMonstre(){
		String[] tab = {"N", "S", "E", "O"} ;
		
		
		for(int i = 0; i < this.tabCase.length ; i++){
			for(int j = 0; j < this.tabCase[i].length ; j++){
				if(this.checkCase(i, j)==3) {
					int nb = (int)((Math.random() * 10) % 4);
					((Personnage)((Case_Personnage) this.tabCase[i][j]).getPersonnage()).change_direction(tab[nb ]);
				}	
			}
		}
	}



	/** 
	 * Méthode parcourant le tableau et affiche chaque case
	 */
	public void afficher(){

		for(int i = 0; i < this.tabCase[0].length ; i++){
			System.out.println();
			for(int j = 0; j < this.tabCase.length ; j++){
				System.out.print(this.tabCase[j][i].toString());
			}
		}
	}
	
	/**
	 * Méthode retournant le tableau de case
	 * @return tabCase
	 */
	public Case[][] getTabCase() {
		return tabCase;
	}

	/**
	 * Méthode permettant d'attribuer le tableau en paramètre à tabCase
	 * @param tabCase
	 */
	public void setTabCase(Case[][] tabCase) {
		this.tabCase = tabCase;
	}
	
	
	

	public void attaquer(Case c){
		
		int x = c.getPosX();
		int y = c.getPosY();
		
		if (this.checkCase(x, y) == 2){
			//rempli un tableau de case, avec les case dans lesquelles ont peut attaquer
			int i = x-2;
			if (i < 0)
				i = 0;
			
			while ( (i< x+3) && (i< this.tabCase.length) ){
				int j = y-2;
				if (j < 0)
					j = 0;
				while ( (j< y+3) && (j < this.tabCase[i].length) ){
					if ( i == x || j == y && !((i==x) && (j==y))) {
						if (this.tabCase[i][j] instanceof Case_Personnage){
							if ( ((Case_Personnage)(this.tabCase[i][j])).getPersonnage() instanceof Monstre){
								((Monstre)((Case_Personnage)(this.tabCase[i][j])).getPersonnage()).getVie().decrementer();
							}
						}

					}
					j++;
				}
				i++;
			}
		}
		
	}
	
	
}
