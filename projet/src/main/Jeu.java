package main;

import java.util.*;

import Case.*;
import Personnage.*;
import Vue.moteurGraphique.gui.*;
/**
 * @author FREI LAMBOLEZ LOGEART  PERES
 * Classe Jeu
 * Classe principale qui modélise le jeu
 */
public class Jeu {
	
	private Aventurier joueur;
	private Labyrinth lab;
	private Monstre enemy;
	
	
	/**
	 * constructeur vide pour le chargement par fichier
	 */
	public Jeu(){
		
	}
	
	public Jeu(String n){

		this.joueur = new Aventurier(n);
		this.enemy = new Monstre(n);
		
		this.lab = new Labyrinth(20, 20);
		
		this.lab.addPerso(4, 4, this.joueur);
		this.lab.addPerso(7, 7, this.enemy);
		
		for(int i=3;i<6;i++){
			this.lab.addMur(i, 2);
		}
		
	}

	public static void main(String [] args){
		
		/* V2 
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Entrez le nom de votre aventurier : ");
		String nom = sc.nextLine();
		
		Jeu lejeu = new Jeu(nom);
		
		try {
			MoteurGraphique mg = new MoteurGraphique(lejeu);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		boolean continuer = true;
		String cmd;
		
		Aventurier a = lejeu.getJoueur();
		Labyrinth l = lejeu.getLab();
		
		while(continuer){
			
			l.afficher();
			System.out.println("Le joueur " + nom + " se situe a la position : ("+a.getEspace().getPosX()+","+a.getEspace().getPosY()+")");
			System.out.println("Entrez la direction de deplacement (N,S,E,O) : ");
			cmd = sc.nextLine();
			
			a.change_direction(cmd);
			
			l.evoluer();
			
		}
		sc.close();
		
		*/
		
		
		/* V3 */
		Backup loader = new Backup("level/default.txt");
		
		Jeu lejeu = loader.load();
		
		
		
		try {
			MoteurGraphique mg = new MoteurGraphique(lejeu);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Aventurier getJoueur() {
		return joueur;
	}

	public void setJoueur(Aventurier joueur) {
		this.joueur = joueur;
	}

	public Labyrinth getLab() {
		return lab;
	}

	public void setLab(Labyrinth lab) {
		this.lab = lab;
	}

	public void deplacerPersonnage(String cmd) {
		this.joueur.change_direction(cmd);
		this.lab.deplacerMonstre();
		this.lab.evoluer();
		
	}
	
	public void setEnemy(Monstre m){
		this.enemy = m;
	}
	
	public Monstre getEnemy(){
		return this.enemy;
	}
	
	public void quitter(){
		
		System.out.println("Save ? (Y/N)");
		Scanner sc=new Scanner(System.in);
		String rep = sc.nextLine();
		if(rep.toUpperCase().startsWith("Y")){
			System.out.println("enter path");
			rep = sc.nextLine();
			if(rep.equals("-")){
				rep="level/defautl_out.txt";
			}
			Backup b = new Backup(rep);
			b.save(this);
			System.exit(0);
		}else{
			System.out.println("Patientez, le jeu va reprendre");
		}
	}
	
	public void attaquerMonstre(){
		this.lab.attaquer(this.joueur.getEspace());
	}
}
