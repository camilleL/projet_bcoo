package Vue.moteurGraphique.gui;

import java.util.Scanner;

import main.*;

public class MoteurGraphique implements Runnable {

	Jeu j;
	Thread runner;

	ControleurGraphique controle;
	Dessineur affiche;

	public MoteurGraphique(Jeu j) throws InterruptedException{
		//creation du jeu
		this.j=j;

		//creation du moteur graphique
		InterfaceGraphique inter=new InterfaceGraphique();
		controle=inter.getControleur();
		affiche=inter.getAfficheur();
		

		this.runner = new Thread(this);
		runner.start();


	}

	@Override
	public void run()  {


		//boucle de jeu
		while(true)
		{
			String c=controle.getCommande();		
			if(c=="esc"){
				j.quitter();
				try {
					runner.sleep(10000);
				} catch (InterruptedException e) { }
			}
			
			if (c=="a"){
				j.attaquerMonstre();
			}
			
			
			j.deplacerPersonnage(c);
			affiche.dessiner(j);
			try {
				runner.sleep(100);
			} catch (InterruptedException e) { }
		}

	}

}
