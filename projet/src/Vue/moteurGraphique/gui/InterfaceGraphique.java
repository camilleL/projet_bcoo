package Vue.moteurGraphique.gui;

import java.awt.*;
import javax.swing.*;
import main.*;


/**
 * cr�e une interface graphique avec son controleur et son afficheur
 * @author vthomas
 *
 */
public class InterfaceGraphique  {

	/**
	 * l'afficheur li� � la JFrame
	 */
	Dessineur dessin;
	
	/**
	 * le controleur li� � la JFrame
	 */
	ControleurGraphique controleur;
	
	
	/**
	 * la construction de l'interface grpahique
	 * - construit la JFrame
	 * - construit les Attributs
	 * 
	 */
	public InterfaceGraphique()
	{
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// creation afficheurGrame()
		AfficheurGraphique affiche=new AfficheurGraphique(500,550);
		this.dessin=new Dessineur(500, 550, affiche);
		f.setContentPane(affiche);
		
		//ajout du controleur
		ControleurGraphique controlleurGraph=new ControleurGraphique();
		this.controleur=controlleurGraph;
		affiche.addKeyListener(controlleurGraph);	
		
		//recuperation du focus
		f.pack();
		f.setVisible(true);
		f.getContentPane().setFocusable(true);
		f.getContentPane().requestFocus();
	}
	
	/**
	 * retourne l'afficheur de l'interface construite
	 */
	public Dessineur getAfficheur() {
		return dessin;
	}
	
	/**
	 * retourne le controleur de l'affichage construit
	 * @return
	 */
	public ControleurGraphique getControleur() {
		return controleur;
	}
	
}
