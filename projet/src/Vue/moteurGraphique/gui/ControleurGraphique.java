package Vue.moteurGraphique.gui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * classe qui repr�sente un controleur en lien avec un KeyListener
 * 
 * @author vthomas
 * 
 */
public class ControleurGraphique implements KeyListener {

	/**
	 * commande en cours
	 */
	String commande;

	/**
	 * construction du controleur par d�faut le controleur n'a pas de commande
	 */
	public ControleurGraphique() {
		this.commande = new String();
	}

	/**
	 * quand on demande les commandes, le controleur retourne la commande en
	 * cours
	 * 
	 * @return commande faite par le joueur
	 */
	public String getCommande() {
		return commande;
	}

	@Override
	/**
	 * met � jour les commandes en fonctions des touches appuy�es
	 */
	public void keyPressed(KeyEvent e) {

		
		switch (e.getKeyChar()) {
		// si on appuie sur 'q',commande joueur est gauche
		case 'q':
			this.commande = "o";
			break;
		// si on appuie sur 'd',commande joueur est droite
		case 'd':
			this.commande = "e";
			break;
		// si on appuie sur 'z',commande joueur est haut
		case 'z':
			this.commande = "n";
			break;
		// si on appuie sur 's',commande joueur est bas
		case 's':
			this.commande= "s";
			break;
		case KeyEvent.VK_ESCAPE:
			this.commande="esc";
			break;
		case KeyEvent.VK_SPACE :
			this.commande = "a";
			break;
		}

	}

	@Override
	/**
	 * met � jour les commandes quand le joueur relache une touche
	 */
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyChar()) {
		case 'q':
			this.commande = " ";
			break;
		case 'd':
			this.commande = " ";
			break;
		case 'z':
			this.commande = " ";
			break;
		case 's':
			this.commande = " ";
			break;
		case KeyEvent.VK_SPACE :
			this.commande = " ";
			break;
		}

	}

	@Override
	/**
	 * ne fait rien
	 */
	public void keyTyped(KeyEvent e) {

	}

}
