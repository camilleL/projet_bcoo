package Vue.moteurGraphique.gui;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import main.*;
import Case.Case;
import Personnage.*;

/**
 * un afficheur graphique
 * 
 * cela fonctionne avec un principe de doubleBuffering - une image vide est cr�e
 * en plus du JPanel - on demande d'ajouter des �l�ments dans cette image vide -
 * on demande ensuite d'afficher cette image dans le JPanel
 * 
 * @author vthomas
 */
public class Dessineur  {

	/**
	 * constante pour g�rer la taille des cases
	 */
	protected int TAILLE_CASE = 25;
	
	
	/**
	 * lien avers l'afficheur dans lequel dessiner
	 */
	AfficheurGraphique affiche; 

	/**
	 * une image intermediaire
	 */
	private BufferedImage imageSuivante;
	private BufferedImage imageEnCours;
	
	
	private Image[] tabImage;

	/**
	 * la taille des images
	 */
	int width, height;

	/**
	 * constructeur vide cela construit une image intermediaire et permet de
	 * dessiner dessus
	 */
	public Dessineur(int width, int height, AfficheurGraphique aff) {
		this.width = width;
		this.height = height;
		this.affiche= aff;

		// cree l'image buffer et son graphics
		this.imageSuivante = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		this.imageEnCours = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		
		
		tabImage=new Image[12];
		try{
			tabImage[0]=ImageIO.read(new File("image/sol.jpg"));			//sol
			tabImage[1]=ImageIO.read(new File("image/mur.jpg"));			//mur
			tabImage[2]=ImageIO.read(new File("image/perso_nord.gif"));		//perso nord
			tabImage[3]=ImageIO.read(new File("image/monstre.gif"));		//monstre 1
			tabImage[4]=ImageIO.read(new File("image/flammes.gif"));		//flammes
			tabImage[5]=ImageIO.read(new File("image/perso_sud.gif"));		//perso sud
			tabImage[6]=ImageIO.read(new File("image/perso_est.gif"));		//perso est
			tabImage[7]=ImageIO.read(new File("image/perso_ouest.gif"));	//perso ouest
			tabImage[8]=ImageIO.read(new File("image/coeurPlein.jpg"));		//image coeur plain
			tabImage[9]=ImageIO.read(new File("image/coeurVide.jpg"));		//image coeur vide
			tabImage[10]=ImageIO.read(new File("image/chest.gif"));			//Coffre
			tabImage[11]=ImageIO.read(new File("image/stairs.png"));		//escalier de sortie
		}catch(IOException e){
			System.out.println("Chargement des images impossible");
		}
		
	}

	/**
	 * dessiner un objet consiste � utiliser un sprite
	 */
	public void dessinerObjet(int val, int x, int y, int taille){
		Graphics2D crayon = (Graphics2D) imageSuivante.getGraphics();
			
		if(val!=4){
			crayon.drawImage(tabImage[0], x * TAILLE_CASE, y * TAILLE_CASE, taille, taille, affiche);
		}
		
		if(val!=-1){
			crayon.drawImage(tabImage[val], x * TAILLE_CASE, y * TAILLE_CASE, taille, taille, affiche);
		}
	}

	/**
	 * On inverse les images celle � dessiner et celle temporaire
	 */
	public void rendreImage() {
		BufferedImage temp = this.imageEnCours;
     	// l'image � dessiner est celle qu'on a construite
		this.imageEnCours = this.imageSuivante;
		// l'ancienne image est vid�e
		this.imageSuivante = temp;
		this.imageSuivante.getGraphics().fillRect(0, 0, this.width,this.height);

		affiche.modifierImage(imageEnCours);
	}

	
	/**
	 * m�thode dessiner � completer
	 * 
	 * @param j
	 *            jeu � dessiner
	 */
	public void dessiner(Jeu lejeu) {
		Personnage pj=lejeu.getJoueur();
		this.rendreImage();
		Case[][] tabCase = lejeu.getLab().getTabCase();
		Labyrinth lab = lejeu.getLab();
		int valcase;
		boolean aAttaquer=false;
		for(int i = 0; i < tabCase[0].length ; i++)
		{
			for(int j = 0; j < tabCase.length ; j++)
			{
				valcase=lab.checkCase(j, i);
				if(valcase==4){
					valcase=10;
				}
				if(valcase==6){
					valcase=11;
				}
				if(valcase==2){
					switch (pj.getDerniereDirection().toUpperCase()) {
					case "N" :
						valcase=2;
						break;
					case "S" :
						valcase=5;
						break;	
					case "E" :
						valcase=6;
						break;
					case "O" :
						valcase=7;
						break;	
					case "A":
						aAttaquer=true;
						
					default:
						valcase=5;
					}
				}
				this.dessinerObjet(valcase,j,i,TAILLE_CASE);
			}
		}
		int vie = pj.getVie().getValeur();
		
		int val;
		for(int i=0;i<5;i++){
			if((i+1)<=vie){
				val=8;
			}else{
				val=9;
			}
			this.dessinerObjet(val,i+1,tabCase.length,TAILLE_CASE);
		}
		if(vie>5){
			for(int i=0;i<5;i++){
				if((i+6)<=vie){
					val=8;
				}else{
					val=9;
				}
				this.dessinerObjet(val,i+1,tabCase.length,TAILLE_CASE);
			}
		}
		if(aAttaquer){
			System.out.println("Attaque !!");
			this.dessinerObjet(4,pj.getEspace().getPosX()-2,pj.getEspace().getPosY()-2,TAILLE_CASE*5);
		}
		
	}

}
